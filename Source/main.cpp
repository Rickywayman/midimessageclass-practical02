//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "MidiMessage.h"


int main (int argc, const char* argv[])
{

    // insert code here...
    std::cout << "Hello, World!\n";
    
    MidiMessage note;
    
    int getNoteNumber(MidiMessage* note);
    
    std::cout << "Note = " << note.getNoteNumber() << std::endl << "Frequency = " << note.getMidiNoteInHertz() << std::endl << "Velocity = " << note.getFloatVelocity() << std::endl;
    int value1;
    std::cout << "Please input a Midi note into the console..";
    std::cin >> value1;
    note.setNoteNumber(value1);
    
    float value2;
    std::cout << "Please input a Midi velocity into the console..";
    std::cin >> value2;
    note.setFloatVelocity(value2);
    
    std::cout << "New Note = " << note.getNoteNumber() << std::endl << "New Frequency = " << note.getMidiNoteInHertz() << std::endl << "New Velocity = " << note.getFloatVelocity() << std::endl;

    
    
    return 0;
}

