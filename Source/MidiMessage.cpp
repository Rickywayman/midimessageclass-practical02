//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Ricky Wayman on 19/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.h"
#include <cmath>


MidiMessage::MidiMessage()
    {
        number = 60;
        velocity = 1.0;
    }
    MidiMessage::~MidiMessage()
    {
        
    }
    void MidiMessage::setNoteNumber (int value1)//Mutator
    {
        number = value1;
    }
    void MidiMessage::setFloatVelocity (float value2)//Mutator
    {
        velocity = value2;
    }
    int MidiMessage::getNoteNumber() const//Assessor
    {
        return number;
    }
    float MidiMessage::getMidiNoteInHertz() const//Assessor
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
    float MidiMessage::getFloatVelocity() const//Assessor
    {
        return velocity;
    }

