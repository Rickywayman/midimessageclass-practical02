//
//  MidiMessage.h
//  CommandLineTool
//
//  Created by Ricky Wayman on 19/10/2015.
//  Copyright (c) 2015 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__MidiMessage__
#define __CommandLineTool__MidiMessage__

#include <stdio.h>

#endif /* defined(__CommandLineTool__MidiMessage__) */

class MidiMessage
{
public:
    MidiMessage();
    
    ~MidiMessage();
    
    void setNoteNumber(int value1);//Mutator
    
    void setFloatVelocity(float value2);//Mutator
    
    int getNoteNumber() const;//Assessor
   
    float getMidiNoteInHertz() const;//Assessor
   
    float getFloatVelocity() const;//Assessor
    
private:
    int number;
    float velocity;
    
};
